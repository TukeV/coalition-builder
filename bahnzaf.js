function calculate_bahnzaf(powers, theshold){
  var swing_total = 0;
  var swings = new Array(powers.length).fill(0);
  var combination = ((1 << powers.length) - 1);
  while(combination > 0){
    var binrep = combination.toString(2).padStart(powers.length);
    var coalition_power = 0;
    var coalition = [];
    for(var i = 0; i < binrep.length; ++i){
      var b = binrep.charAt(i);
      if (b == '1'){
        coalition.push(i);
        coalition_power += powers[i];
      }
    }
    if(coalition_power >= theshold){
      for(var ci in coalition){
        var party = coalition[ci];
        if(coalition_power - powers[party] < theshold){
          ++swing_total;
          ++swings[party];
        }
      }
    }
    --combination;
  }
  var bahnzaf_indices = [];
  for(var swing_count in swings){
    bahnzaf_indices.push(swings[swing_count] / swing_total);
  }
  return bahnzaf_indices;
};