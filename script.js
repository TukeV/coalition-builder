var all_parties = {};
var govPower = 0;
var oppPower = 0;

/* Modal */
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

// When party is added
function onPartyAdded() {
  var party = {
    "name": document.getElementById('partyName').value,
    "power": Number(document.getElementById('partyPower').value),
    "color": document.getElementById('partyColor').value,
    "gov": false,
  };
  oppPower += party.power;
  all_parties[party.name] = party;
  addPartyGraphics(party);
};

/* Graphics */
// Add new party ball

function addPartyGraphics(party){
  // Add Ball
  var partyBall = document.createElement("div");
  partyBall.className = "party";
  partyBall.id = party.name;
  partyBall.innerHTML = party.name + "</br>" + party.power;
  // Resize and colorize the ball to match the party's popularity.
  var radius = Math.sqrt(party.power*4000/Math.PI);
  partyBall.style.height = radius+'px'; 
  partyBall.style.width = radius+'px';
  partyBall.style.backgroundColor = party.color;
  // Add callbacks for moving the ball
  partyBall.addEventListener('mousedown', handleDrag, true);
  partyBall.addEventListener('dragstart', startDragging, true);
  var parent = document.getElementById("container");
  parent.appendChild(partyBall);
  // Add Row
  var body = document.getElementById("side-panel-body");
  var row = body.insertRow(-1);
  row.insertCell(-1);
  row.insertCell(-1);
  row.insertCell(-1);
  row.insertCell(-1);
  row.insertCell(-1);
  // Update Bahnzaf
  update_power_indices();
};


function populate_table(){
  var party_list = [];
  for(var party in all_parties){
    party_list.push(party)
  }
  party_list.sort(function(a, b){ return a.power - b.power})
}


// Drag the ball
function handleDrag(event){
  let ball = event.target;
  let shiftX = event.clientX - ball.getBoundingClientRect().left;
  let shiftY = event.clientY - ball.getBoundingClientRect().top;

  ball.style.position = 'absolute';
  ball.style.zIndex = 1000;
  document.body.append(ball);

  moveAt(event.pageX, event.pageY);

  // centers the ball at (pageX, pageY) coordinates
  function moveAt(pageX, pageY) {
    ball.style.left = pageX - shiftX + 'px';
    ball.style.top = pageY - shiftY + 'px';
  }

  function onMouseMove(event) {
    moveAt(event.pageX, event.pageY);
  }

  // (3) move the ball on mousemove
  document.addEventListener('mousemove', onMouseMove);

  // (4) drop the ball, remove unneeded handlers
  ball.onmouseup = function() {
    document.removeEventListener('mousemove', onMouseMove);
    ball.onmouseup = null;
    updatePower(ball);
  };
};

function startDragging(){
  return false;
};

function updatePower(currentElement){
  var opposition_area = document.getElementById("opposition");
  var government_area = document.getElementById("government");
  var rect1 = currentElement.getBoundingClientRect();
  var rect2 = government_area.getBoundingClientRect();
  var rect3 = opposition_area.getBoundingClientRect();
  var inGov = !(rect1.right < rect2.left || 
                rect1.left > rect2.right || 
                rect1.bottom < rect2.top || 
                rect1.top > rect2.bottom);

  var inOpp = !(rect1.right < rect3.left || 
                rect1.left > rect3.right || 
                rect1.bottom < rect3.top || 
                rect1.top > rect3.bottom);

  var party = all_parties[currentElement.id];
  if(inGov && !party.gov){
    govPower += party.power;
    oppPower -= party.power;
    party.gov = true;
  }else if(inOpp && party.gov){
    govPower -= party.power;
    oppPower += party.power;
    party.gov = false;
  }else if(!inOpp && !inGov){
    party.gov ? govPower -= party.power : oppPower -= party.power;
    removeParty(party);
  }
  var totalPower = oppPower + govPower;
  var oppPer = ((oppPower/totalPower)*100).toFixed(1);
  var govPer = ((govPower/totalPower)*100).toFixed(1);
  document.getElementById("oppositionPower").innerHTML = oppPer+'%';
  document.getElementById("governmentPower").innerHTML = govPer+'%';
};

function removeParty(party){
  var ball = document.getElementById(party.name);
  ball.parentNode.removeChild(ball);
  delete all_parties[party.name];
  update_power_indices();
}

function parties2list(){
  var party_list = [];
  for(var party in all_parties){
    party_list.push(all_parties[party])
  }
  party_list.sort(function(a, b){ return b.power - a.power});
  return party_list;
}

function update_power_indices(){
  var sorted_parties = parties2list();
  var powers = [];
  var total_power = 0;
  for(var i = 0; i < sorted_parties.length; ++i){
    var party = sorted_parties[i];
    total_power += party.power;
    powers.push(party.power);
  }
  var bahnzaf_indices = calculate_bahnzaf(powers, Math.ceil(total_power/2));
  var body = document.getElementById("side-panel-body");
  for(var i = 0; i < sorted_parties.length; ++i){
    var party = sorted_parties[i];
    var row = body.rows[i];
    row.cells[0].innerHTML = party.name;
    row.cells[1].innerHTML = party.power;
    row.cells[2].innerHTML = ((party.power/total_power)*100).toFixed(1) + '%';
    row.cells[3].innerHTML = bahnzaf_indices[i].toFixed(3);
    row.cells[4].innerHTML = (bahnzaf_indices[i] - (party.power/total_power)).toFixed(3);
    row.style.backgroundColor = party.color;
  }
};
